<section id="comments">
  <h4 class="title"><?php print t('Comments on this Post'); ?></h4>
  <?php print render($content['comments']); ?>
  <?php if ($content['comment_form']): ?>
    <section id="comment-form-wrapper" class="well">
      <h2 class="title"><?php print t('Add new comment'); ?></h2>

      <div class="user-image">
        <?php
        if($logged_in) {
        $user = user_load($user->uid);
        print theme('image_style', array('style_name' => 'userpic', 'path' => $user->picture->uri));
}
?>
</div>

      <?php print render($content['comment_form']); ?>
    </section> <!-- /#comment-form-wrapper -->
  <?php endif; ?>
</section> <!-- /#comments -->
